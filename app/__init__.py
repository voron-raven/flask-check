from flask import Flask, url_for
from flask_debugtoolbar import DebugToolbarExtension
import os

app = Flask(__name__)

# Determines the destination of the build. Only usefull if you're using Frozen-Flask
app.config['FREEZER_DESTINATION'] = os.path.dirname(os.path.abspath(__file__))+'/../build'

# Function to easily find your assets
# In your template use <link rel=stylesheet href="{{ static('filename') }}">
app.jinja_env.globals['static'] = (
    lambda filename: url_for('static', filename = filename)
)

from app import views

# the toolbar is only enabled in debug mode:
app.debug = True

# set a 'SECRET_KEY' to enable the Flask session cookies
app.config['SECRET_KEY'] = 'A0Zr98j/3yX I~XHH!jmN]LWX/,?RT'

toolbar = DebugToolbarExtension(app)
