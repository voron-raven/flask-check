import os, sys
BASE_TEMPLATES_DIR = os.path.dirname(os.path.abspath(__file__)) + '/templates'

@app.context_processor
def categories():
    """Get all tools."""
    tools = []
    if os.path.exists(BASE_TEMPLATES_DIR + '/tools'):
        tools.extend([f for f in os.listdir(BASE_TEMPLATES_DIR + '/tools') if (os.path.isfile(os.path.join(BASE_TEMPLATES_DIR + '/tools', f)) and  f.endswith(".html"))])

    tools = [{'name': f[:-5].replace('-', ' '), 'slug': f[:-5]} for f in tools]

    return {'tools': tools}


@app.context_processor
def select_parent_template():
    """Check if it's ajax, if so no need to parent template."""
    parent_template = "dummy_parent.html" if request.is_xhr else "base.html"
    return {'parent_template': parent_template}


@app.context_processor
def openshift():
    """Check if it's openshift."""
    return {'OPENSHIFT': ('OPENSHIFT_APP_NAME' in os.environ)}
